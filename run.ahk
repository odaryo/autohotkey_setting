;;
;; An autohotkey script that provides emacs-like keybinding on Windows
;;
#InstallKeybdHook
#UseHook

SetKeyDelay 0

; キーバインドを無効にしたいウィンドウ一覧
; 必要の無い部分はコメントアウト
is_emacs_target()
{
;  IfWinActive,ahk_class VirtualConsoleClass ; ConEmu
;    Return 1 
  IfWinActive,ahk_class MEADOW ; Meadow
    Return 1 
  IfWinActive,ahk_class cygwin/x X rl-xterm-XTerm-0
    Return 1
  IfWinActive,ahk_class Vim ; GVIM
    Return 1
  IfWinActive,ahk_class Emacs ; Emacs
    Return 1
  IfWinActive,ahk_class VTWin32 ; Tera Term VT
    Return 1
;  IfWinActive,ahk_class SWT_Window0 ; Eclipse
;    Return 1
  Return 0
}

cut_line()
{
  Send {ShiftDown}{END}{SHIFTUP}
  Sleep 50 ;[ms] this value depends on your environment
  Send ^x
  Return
}

new_line()
{
	Send {END}{Enter}
	Return
}

^l::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send {Right}
  Return
+^l::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send +{Right}
  Return
^k::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send {Left}
  Return
+^k::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send +{Left}
  Return
^p::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send {Up}
  Return
+^p::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send +{Up}
  Return
^n::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send {Down}
  Return
+^n::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send +{Down}
  Return
^j::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send {Home}
  Return
+^j::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send +{Home}
  Return
^;::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send {End}
  Return
+^;::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send +{End}
  Return
^d::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    Send {DEL}
  Return
^i::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    cut_line()
  Return
^o::
  If is_emacs_target()
    Send %A_ThisHotkey%
  Else
    new_line()
  Return
F1::Return
